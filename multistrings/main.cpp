#include <iostream>
#include "constant.h"
#include "../exc/PXT_Explained/step_06/pxt.h"


void outa();
void outb();

int main() 
{
    std::cout << "Hello, World!" << std::endl;

    std::cout <<"main:" << STR << std::endl;

    outa();
    outb();

    PX(STR);
    PT(decltype(STR));

    return 0;
}
