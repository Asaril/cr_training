#ifndef clock2_Clock_H
#define clock2_Clock_H

#include "ChainCounter.h"

namespace clock2 {

    using HourCounter = ChainCounter<NoCounter, 24>;
    using MinuteCounter = ChainCounter<HourCounter, 60>;
    using SecondCounter = ChainCounter<MinuteCounter, 60>;

    class Clock {
        NoCounter dummy;
        HourCounter hour;
        MinuteCounter minute;
        SecondCounter second;
    public:
        Clock()
            : hour{dummy}
            , minute{hour}
            , second{minute}
        {} 
        auto getSeconds() const { return second.get(); }
        auto getMinutes() const { return minute.get(); }
        auto getHours() const { return hour.get(); }
        auto stepSeconds(int s = 1) { second.count(s); }
        auto stepMinutes(int s = 1) { minute.count(s); }
        auto stepHours(int s = 1) { hour.count(s); } 
    };
} // namepace clock2

#endif // clock2_Clock_H
