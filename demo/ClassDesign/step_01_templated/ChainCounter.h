#ifndef clock2_ChainCounter_H
#define clock2_ChainCounter_H

namespace clock2 {
    class NoCounter {
        public:
        void count(int = 0) {}
    };

    template<typename TCTR, const int TLIMIT>
    class ChainCounter {
        int count_{0};
        TCTR& next_;
    public:
        ChainCounter(TCTR& next)
            : next_{next}
        {}
        auto get() const { return count_; }
        void count(int step = 1);
    };

    template <typename TCTR, const int TLIMIT>
    void ChainCounter<TCTR, TLIMIT>::count(int s) {
        for (;s > 0; --s) {
            if (++count_ >= TLIMIT) {
                count_ = 0;
                next_.count();
            }
        } 
    }    
}

#endif // clock2_ChainCouner