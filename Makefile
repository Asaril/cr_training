#CXX=g++
CXXFLAGS=-g -O0
#LDFLAGS=
BUILD=build

$(BUILD)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(BUILD)/training: $(BUILD)/main.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

all: $(BUILD)/training

clean:
	rm $(BUILD)/* || true